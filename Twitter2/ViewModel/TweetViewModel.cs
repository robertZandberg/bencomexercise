﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tweetinvi;
using TwitterApplication.ViewModel;

namespace TwitterApplication.ViewModel
{
    public class TweetViewModel
    {
        public DateTime Created_At { get; set; }
        public HtmlString Text { get; set; }
        public List<string> Images { get; set; }
        public List<string> Hashtag { get; set; }
        public string Created_By { get; set; }
    }
}