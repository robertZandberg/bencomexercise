﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tweetinvi;
using TwitterAPI;
using Tweet = TwitterApplication.ViewModel.TweetViewModel;

namespace TwitterApplication.Controllers
{
    public class HomeController : Controller
    {
      
        public ActionResult Index()
        {
            return View();
        }

        
       
        [HttpGet]
        public ActionResult Search(string item)
        {
        
            var twitterMapper = new TwitterMapper();
            var viewMapper = new Mapper.Mapper();

            
            var model = viewMapper.MapTweetsToViewModel(twitterMapper.GetTweetList(item.ToLower()));

            return View(model);
        }

    }
}