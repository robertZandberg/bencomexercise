﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TwitterApplication.ViewModel;
using TwitterAPI;
using TwitterAPI.Models;

namespace TwitterApplication.Mapper
{
    public class Mapper
    {
        public List<TweetViewModel> MapTweetsToViewModel(List<TweetDataModel> tweets)
        {
            var tweetList = new List<TweetViewModel>();
            foreach (var tweetDataModel in tweets)
            {
                var tweet = new TweetViewModel()
                {
                    Created_At = tweetDataModel.Created_At,
                    Created_By = tweetDataModel.Created_By,
                    Hashtag = tweetDataModel.Hashtag,
                    Images = tweetDataModel.Images,
                    Text =  new HtmlString(tweetDataModel.Text)
                };
                tweetList.Add(tweet);
            }

            return tweetList;
        }
    }
}