﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
namespace TwitterAPI.Models
{
    public class TweetDataModel
    {
        public DateTime Created_At { get; set; }
        public Int64 Id { get; set; }
        public string Id_str { get; set; }
        public string Text { get; set; }
        public string Source { get; set; }
        public List<string> Images { get; set; }
        public List<string> Hashtag { get; set; }
        public string Created_By { get; set; }
        public string FullText { get; set; }
        public string Country { get; set; }
        public string City { get; set; }





    }
}
