﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TwitterAPI.Models;

namespace TwitterAPI
{
    public class TwitterMapper
    {
        private List<TweetDataModel> tweetsList = new List<TweetDataModel>(); 

        public void SetList(string tweetName)
        {
            var result = TwitterConfig.SearchForTweets(tweetName);
            foreach (var tweet in result)
            {
                var x = new TweetDataModel
                {
                    Created_At = tweet.CreatedAt,
                    Id = tweet.Id,
                    Id_str = tweet.IdStr,
                    Source = tweet.Source,
                    Text = tweet.Text,
                    Images = tweet.Media.ConvertAll(d=> d.MediaURL),
                    Hashtag = tweet.Hashtags.ConvertAll(h=> h.Text),
                    Created_By = tweet.CreatedBy?.Name,
                    FullText = tweet.FullText,
                    Country = tweet.Place?.Country,
                    City = tweet.Place?.Name
                };
                tweetsList.Add(x);
            }
        }

        public List<TweetDataModel> GetTweetList(string searchName)
        {
            SetList(searchName);
            return tweetsList;
        }



    }
}
